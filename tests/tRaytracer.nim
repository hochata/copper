import copperpkg/[raytracing, space, canvas]

discard """
  cmd: "nim $target --hints:on -d:testing $options $file"
"""

block intersection:
  let s = sphere(vec3(0, 0, 1), 1, color(0, 255, 0))
  let sols = s.intersect(vec3(0, 0, 0), vec3(0, 0, 1))
  doAssert 0 in sols
  doAssert 2 in sols
